# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import selenium
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class UntitledTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        self.base_url = "http://172.24.7.33/login"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_untitled_test_case(self):
        driver = self.driver
        driver.get("http://172.24.7.33/login")
        driver.find_element_by_id("account").clear()
        driver.find_element_by_id("account").send_keys("TEST0")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST0")
        driver.find_element_by_xpath("//button[@type='button']").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div/a[2]/div/div[2]/div[3]/a/centerstyle/div").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div[5]").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div/span/span").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div/span/div/div/div/div/div/div/div/div[2]/div[2]/table/tbody/tr/td[3]/div").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div/span/div/div/div/div/div/div/div/div[2]/div[2]/table/tbody/tr[2]/td[3]/div").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | xpath=(//button[@value='B'])[2] | ]]
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | xpath=(//button[@value='A'])[2] | ]]
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='N'])[2]").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-enabled.ant-select-focused > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li[4]").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-focused.ant-select-enabled > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li[5]").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-focused.ant-select-enabled > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li[6]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div/div/div[2]/div[2]/div/div").click()
        #driver.find_element_by_xpath("//div[@id='5ecef1f2-d657-4571-8ab3-9eabe12d3bea']/ul/li[3]").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-focused.ant-select-enabled > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='5ecef1f2-d657-4571-8ab3-9eabe12d3bea']/ul/li[4]").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-focused.ant-select-enabled > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='5ecef1f2-d657-4571-8ab3-9eabe12d3bea']/ul/li").click()
        #driver.find_element_by_css_selector("div.ant-select.ant-select-enabled.ant-select-focused > div.ant-select-selection.ant-select-selection--single > span.ant-select-arrow > i.anticon.anticon-down.ant-select-arrow-icon > svg").click()
        #driver.find_element_by_xpath("//div[@id='392443b9-0a01-40db-f481-4ab34bf45e75']/ul/li").click()
        driver.find_element_by_id("prescriptionorderinput").click()
        driver.find_element_by_id("prescriptionorderinput").clear()
        driver.find_element_by_id("prescriptionorderinput").send_keys("a")
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='a4c2fc23-d9cc-4ea7-cfe4-46d864b755ad']/ul/li[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='a4c2fc23-d9cc-4ea7-cfe4-46d864b755ad']/ul/li[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='a4c2fc23-d9cc-4ea7-cfe4-46d864b755ad']/ul/li[4]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='a4c2fc23-d9cc-4ea7-cfe4-46d864b755ad']/ul/li[5]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='745314e7-bf2f-4fb4-b10b-e783d86269e9']/ul/li[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div[2]/div/div").click()
        #driver.find_element_by_xpath("//div[@id='745314e7-bf2f-4fb4-b10b-e783d86269e9']/ul/li[4]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div[2]/div/div").click()
        #driver.find_element_by_xpath("//div[@id='745314e7-bf2f-4fb4-b10b-e783d86269e9']/ul/li").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[5]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div").click()
        #driver.find_element_by_xpath("//div[@id='a4c2fc23-d9cc-4ea7-cfe4-46d864b755ad']/ul/li").click()
        driver.find_element_by_css_selector("svg").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
