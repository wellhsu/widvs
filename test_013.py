# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import selenium
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class UntitledTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = selenium.webdriver.Chrome('chromedriver')
        self.driver.implicitly_wait(30)
        self.base_url = "http://172.24.7.33/login"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_untitled_test_case(self):
        driver = self.driver
        driver.get("http://172.24.7.33/login")
        driver.find_element_by_id("account").clear()
        driver.find_element_by_id("account").send_keys("TEST0")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST0")
        driver.find_element_by_xpath("//button[@type='button']").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div/a[2]/div/div[2]/div[3]/a/centerstyle/div").click()
        driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div[3]").click()
        #driver.find_element_by_xpath("(//input[@value='2020/06/18'])[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | //div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2] | ]]
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[2]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div[2]/table/tbody/tr[2]/td[3]/div").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | //div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3] | ]]
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div/div/a[3]").click()
        #driver.find_element_by_xpath("//div[@id='root']/div/div/div[2]/div/div[2]/div[3]/div[3]/div[2]/div/div/span/div[2]/div/div/div/div/div[2]/div[2]/table/tbody/tr/td[3]/div").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | xpath=(//button[@value='B'])[2] | ]]
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='B'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [doubleClick | xpath=(//button[@value='A'])[2] | ]]
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='A'])[2]").click()
        driver.find_element_by_xpath("(//button[@value='N'])[2]").click()
        driver.find_element_by_css_selector("svg").click()
        time.sleep(3)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
